@extends('layouts.app')

@section('title')
{{$link->judul}}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>{{$link->judul}}</h3></div>
                <div class="card-body">
                @if(cekDate($link->tanggal))
                    <div class="row">
                        <div class="col-6"><strong>Waktu Aktif Zoom :</strong></div>
                        <div class="col-6">{{date("d-m-Y H:i:s", strtotime($link->tanggal))}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>ID Client Zoom :</strong></div>
                        <div class="col-6">{{($link->clientid)?$link->clientid:'-'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>Passcode Zoom :</strong></div>
                        <div class="col-6">{{($link->passcode)?$link->passcode:'-'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>Link Zoom :</strong></div>
                        <div class="col-6"><a href="{{$link->url}}">{{$link->url}}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            Anda akan otomatis dialihkan dalam 5 detik ke link zoom.
                        </div>
                    </div>
                @else
                    <div class="text-center">
                        <h3 class="text-danger">Maaf untuk zoom belum bisa digunakan saat ini</h3>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>Waktu Aktif Zoom :</strong></div>
                        <div class="col-6">{{date("d-m-Y H:i:s", strtotime($link->tanggal))}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>ID Client Zoom :</strong></div>
                        <div class="col-6">{{$link->clientid}} <br>
                            <small class="text-danger">Sebelum jam pelaksanaan, <strong>ID dapat berubah!</strong></small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>Passcode Zoom :</strong></div>
                        <div class="col-6">{{$link->passcode}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6"><strong>Link Zoom :</strong></div>
                        <div class="col-6">
                            <a href="#">{{$link->url}}</a><br>
                            <small class="text-danger">Sebelum jam pelaksanaan, <strong>Link dapat berubah!</strong></small>
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div><br><br><br>
    <div class="row justify-content-center">
        <div class="col-md-10 text-center">
            <h5>tempat Logo-logo sponsor jika ingin dimuat dalam halaman ini</h5>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <img src="https://raw.githubusercontent.com/arifpujin/Exersice1/master/fotoarif.png" alt="logo arif" class="img-fluid col">
                    <img src="https://raw.githubusercontent.com/arifpujin/Exersice1/master/fotoarif.png" alt="logo arif" class="img-fluid col">
                    <img src="https://raw.githubusercontent.com/arifpujin/Exersice1/master/fotoarif.png" alt="logo arif" class="img-fluid col">
                    <img src="https://raw.githubusercontent.com/arifpujin/Exersice1/master/fotoarif.png" alt="logo arif" class="img-fluid col">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@if (cekDate($link->tanggal))
@push('js')
<script>
window.setTimeout(function(){
window.location.href = "{{$link->url}}";
}, 5000);
</script>
@endpush
@endif
