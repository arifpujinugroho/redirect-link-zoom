<?php

namespace App\Models;

use App\Traits\IdIsUuid;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use IdIsUuid;
}
