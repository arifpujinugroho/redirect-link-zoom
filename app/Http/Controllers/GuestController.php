<?php

namespace App\Http\Controllers;

use App\Http\Requests\SponsorRequest;
use App\Models\Link;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    //
    public function index()
    {
        return view('front');
    }

    public function clientRedirect($link)
    {
        $link = Link::where('short',$link)->firstOrFail();
        return view('client',compact('link'));
    }

    public function getSponsor()
    {
        $data = Sponsor::all();
        return response()->json($data, 200)
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function viewAddFile()
    {
        return view('upload');
    }

    public function uploadFIle(SponsorRequest $request)
    {
        $path = $request->file('avatar')->store(
            'avatars/'.$request->user()->id, 's3'
        );
    }
}
